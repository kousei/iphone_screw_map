Name
====
iphone screw_map

Overview
I am annoyed by a small screw when I disassemble iphone.
I thought how to solve it.

## Description
iphone screw_map

## Requirement
https://www.sketch.com/

## Usage
1. Download pdf
2. Print it
3. Stick the Double-sided tape on Circle
4. Put the removed screw on the circle
## caution
Disassembling your iPhone device is also not covered under the limited warranty provided by Apple.
This is rather obvious.

## Contribution
There is only version of iphone 6 plus.
This file is still incomplete.
I am waiting for the cooperation of someone with heart.

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[kousei.tokuhara](htts://www.tegos.co.jp)